package com.address.exception;

/**
 * thrown when postcode is empty or null
 */
public class PostCodeException extends RuntimeException {

    public PostCodeException(String message) {
        super(message);
    }
}
