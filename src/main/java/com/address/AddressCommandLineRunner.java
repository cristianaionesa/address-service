package com.address;

import com.address.service.AddressService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Command Line Runner class for this Spring Application.
 *
 */
@Component
public class AddressCommandLineRunner implements CommandLineRunner {
    private static Log LOGGER = LogFactory.getLog(AddressCommandLineRunner.class);

    @Value("#{'${address.service.postcode.list}'.split(',')}")
    private List<String> postCodes;

    private AddressService addressService;

    @Autowired
    public AddressCommandLineRunner(@Qualifier("xSoftwareAddressService") AddressService addressService) {
        this.addressService = addressService;
    }

    /**
     * retrieves addresses for the specified postcodes and displays them on the console
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("================================================");
        LOGGER.info("");
        LOGGER.info("");

        postCodes.forEach(p -> LOGGER.info(p + " - " + addressService.getAddresses(p)));

        LOGGER.info("");
        LOGGER.info("================================================");
    }
}
