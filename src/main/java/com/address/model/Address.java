package com.address.model;

/**
 * Data model for the application. This is the only data type that the client for AddressService will deal with.
 */
public class Address {
    private String streetName;

    private String placeName;

    public Address(String streetName, String placeName) {
        this.streetName = streetName;
        this.placeName = placeName;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getPlaceName() {
        return placeName;
    }

    @Override
    public String toString() {
        return new org.apache.commons.lang.builder.ToStringBuilder(this)
                .append("streetName", streetName)
                .append("placeName", placeName)
                .toString();
    }
}
