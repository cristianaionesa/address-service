package com.address.service;

import com.address.model.Address;

import java.util.List;

/**
 * all addressService implementations should implement this
 */
public interface AddressService {

    List<Address> getAddresses(String postCode);
}
