package com.address.service.impl.xSoftware.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * data transfer object used by 3xSoftware address API
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Id",
        "StreetAddress",
        "Place"
})
public class PlaceSummary {

    @JsonProperty("StreetAddress")
    private String streetAddress;

    @JsonProperty("Place")
    private String place;
    private Integer id;

    public PlaceSummary(String streetAddress, String place, Integer id) {
        this.streetAddress = streetAddress;
        this.place = place;
        this.id = id;
    }

    public PlaceSummary() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }


}
