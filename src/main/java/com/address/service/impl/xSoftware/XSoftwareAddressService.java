package com.address.service.impl.xSoftware;

import com.address.exception.PostCodeException;
import com.address.model.Address;
import com.address.service.AddressService;
import com.address.service.impl.xSoftware.client.XSoftwareAddressClient;
import com.address.service.impl.xSoftware.dto.AddressInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * AddressService implementation based on 3Xsoftware API
 */

@Service("xSoftwareAddressService")
public class XSoftwareAddressService implements AddressService {

    private XSoftwareAddressClient xSoftwareClient;

    @Autowired
    public XSoftwareAddressService(XSoftwareAddressClient xSoftwareClient) {
        this.xSoftwareClient = xSoftwareClient;
    }

    /**
     * uses 3xSoftware API to retrieve a list of addresses based on a postcode
     * @param postCode
     * @return List of Address for this postcode
     */
    @Override
    public List<Address> getAddresses(String postCode) {

        if (StringUtils.isEmpty(postCode)) {
            throw new PostCodeException("postcode should not be empty");
        }

        AddressInfo responseEntity = xSoftwareClient.getAddressInfo(postCode);

        return responseEntity
                .getSummaries()
                .stream()
                .map(s -> new Address(s.getStreetAddress(), s.getPlace()))
                .collect(Collectors.toList());

    }
}
