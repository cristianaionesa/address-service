package com.address.service.impl.xSoftware.client;

import com.address.service.impl.xSoftware.dto.AddressInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


/**
 * Created by criss on 2/22/17.
 */

@Component
public class XSoftwareAddressClient {
    public static Log LOGGER = LogFactory.getLog(XSoftwareAddressClient.class);
    private RestTemplate restTemplate;

    @Value("${3xSoftware.address.service.url}")
    private String addressServiceUrl;

    @Value("${3xSoftware.address.service.username}")
    private String addressServiceUsername;

    @Value("${3xSoftware.address.service.api.key}")
    private String addressServiceApiKey;

    @Autowired
    public XSoftwareAddressClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public AddressInfo getAddressInfo(String postCode) {
        return restTemplate.getForObject(fullServiceUrl(postCode), AddressInfo.class);
    }

    private String fullServiceUrl(String postCode) {
        StringBuilder addressBuilder = new StringBuilder(addressServiceUrl)
                .append("?")
                .append("username=")
                .append(addressServiceUsername)
                .append("&")
                .append("key=")
                .append(addressServiceApiKey)
                .append("&")
                .append("postcode=%s");

        String fullServiceUrl = String.format(addressBuilder.toString(), postCode);

        LOGGER.debug(fullServiceUrl);
        return fullServiceUrl;
    }
}
