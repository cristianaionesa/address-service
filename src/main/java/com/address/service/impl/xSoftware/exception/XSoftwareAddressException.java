package com.address.service.impl.xSoftware.exception;

/**
 * thrown when there is a problem calling xSoftware API
 */
public class XSoftwareAddressException extends RuntimeException {

    public XSoftwareAddressException(String message) {
        super(message);
    }
}
