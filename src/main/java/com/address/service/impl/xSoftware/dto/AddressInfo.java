package com.address.service.impl.xSoftware.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * data transfer object used by 3xSoftware address API
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Summaries"
})
public class AddressInfo {

    public AddressInfo(List<PlaceSummary> summaries) {
        this.summaries = summaries;
    }

    public AddressInfo() {
    }

    @JsonProperty("Summaries")
    private List<PlaceSummary> summaries;

    public List<PlaceSummary> getSummaries() {
        return summaries;
    }

    public void setSummaries(List<PlaceSummary> summaries) {
        this.summaries = summaries;
    }

}
