package com.address.service.impl.xSoftware;

import com.address.exception.PostCodeException;
import com.address.model.Address;
import com.address.service.impl.xSoftware.client.XSoftwareAddressClient;
import com.address.service.impl.xSoftware.dto.AddressInfo;
import com.address.service.impl.xSoftware.dto.PlaceSummary;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import static org.junit.Assert.*;

/**
 * Created by criss on 2/21/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class XSoftwareAddressServiceTest {
    private static final String WRONG_POSTCODE = "WRO NG";
    private static final String GOOD_POSTCODE = "GO OD";

    @Mock
    private XSoftwareAddressClient xSoftwareClient;

    @InjectMocks
    private XSoftwareAddressService instance;



    @Test(expected = PostCodeException.class)
    public void whenPostcodeIsEmpty_PostCodeExceptionIsThrown() {
        instance.getAddresses("");
    }

    @Test(expected = PostCodeException.class)
    public void whenPostcodeIsNull_PostCodeExceptionIsThrown() {
        instance.getAddresses(null);
    }



    @Test
    public void whenPostcodeNotFound_emptyListIsReturned() {
        AddressInfo addressInfo = new AddressInfo(Collections.emptyList());

        when(xSoftwareClient.getAddressInfo(WRONG_POSTCODE)).thenReturn(addressInfo);

        List<Address> addresses = instance.getAddresses(WRONG_POSTCODE);

        assertEquals(0, addresses.size());
    }

    @Test
    public void whenPostcodeIsFound_correctAddressIsReturned() {
        PlaceSummary place1 = new PlaceSummary("streetAddress1", "place1", 1);
        PlaceSummary place2 = new PlaceSummary("streetAddress2", "place2", 2);

        AddressInfo addressInfo = new AddressInfo(Arrays.asList(place1, place2));

        when(xSoftwareClient.getAddressInfo(GOOD_POSTCODE)).thenReturn(addressInfo);

        List<Address> addresses = instance.getAddresses(GOOD_POSTCODE);

        assertEquals(2, addresses.size());
        assertEquals("streetAddress1", addresses.get(0).getStreetName());
    }
}
